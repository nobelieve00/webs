package th.ac.tu.siit.lab8webservice;
import java.io.*;
import java.net.*;
import java.text.SimpleDateFormat;
import java.util.*;
import org.json.*;
import android.os.*;
import android.app.*;
import android.net.*;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.*;

public class MainActivity extends ListActivity {
	List<Map<String,String>> list;
	SimpleAdapter adapter;
	Long lastUpdate = 0l;
	String current_Prov = "bangkok";
	String curret_title = "Bangkok";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//Do not allow the user to change the orientation of the application 
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
		list = new ArrayList<Map<String,String>>();
		
		File infile = getBaseContext().getFileStreamPath("province.dat");
		if (infile.exists()) {
			try {
				BufferedReader reader = new BufferedReader( new FileReader (infile));
				current_Prov = reader.readLine().trim();
				curret_title = reader.readLine().trim();
				reader.close();
			} catch (Exception e) {
				//Do nothing
				 current_Prov = "bangkok";
				 curret_title = "Bangkok";
			}
		}
		adapter = new SimpleAdapter(this, list, R.layout.item, 
				new String[] {"name", "value"}, 
				new int[] {R.id.tvName, R.id.tvValue});
		setListAdapter(adapter);
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		if(outState != null){
			super.onSaveInstanceState(outState);
		}
		
	}

	
	@Override
	protected void onStart() {
		super.onStart();
		refresh(5,current_Prov);
	}



	public void refresh(int call_min, String c){
		
		try {
			FileOutputStream outfile = openFileOutput("province.dat", MODE_PRIVATE);
			PrintWriter p = new PrintWriter(outfile);
			p.write(current_Prov+"\n");
			p.write(curret_title+"\n");
			
			p.flush(); p.close();
			outfile.close();
		} catch (FileNotFoundException e) {
			Toast t = Toast.makeText(this, "Error: Unable to save data", 
					Toast.LENGTH_SHORT);
			t.show();
		} catch (IOException e) {
			Toast t = Toast.makeText(this, "Error: Unable to save data", 
					Toast.LENGTH_SHORT);
			t.show();
		}
		
		//Check if the device has Internet connection
		ConnectivityManager mgr = (ConnectivityManager)
				getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = mgr.getActiveNetworkInfo();
		if (info != null && info.isConnected()) {
			//Load data when there is a connection
			long current = System.currentTimeMillis();
			if (current - lastUpdate > call_min*60*1000) {
				WeatherTask task = new WeatherTask(this);
				task.execute("http://cholwich.org/"+c+".json");
			}
		}
		else {
			Toast t = Toast.makeText(this, 
					"No Internet Connectivity on this device. Check your setting", 
					Toast.LENGTH_LONG);
			t.show();
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
		case R.id.action_refresh:
			refresh(3,current_Prov);
			break;
		case R.id.action_bangkok:
			int r = 0;
			if (current_Prov == "bangkok")
				r = 5;
			
				current_Prov = "bangkok";
				curret_title = "Bangkok";
				refresh(r,current_Prov);
			
			break;
		case R.id.action_non:
			int r1 = 0;
			if (current_Prov == "nonthaburi")
				r1 = 5;
			
				current_Prov = "nonthaburi";
				curret_title = "Nonthaburi";
			
			refresh(r1,current_Prov);
			break;
		case R.id.action_pathum:
			
			int r2 = 0;
			if (current_Prov == "pathum thani")
				r2 = 5;
			
			current_Prov = "pathum thani";
			curret_title = "Pathum Thani";
			refresh(r2,current_Prov);
			break;
		}
		
		
		return super.onOptionsItemSelected(item);
	}


	class WeatherTask extends AsyncTask<String, Void, String> {
		Map<String,String> record;
		ProgressDialog dialog;
		
		public WeatherTask(MainActivity m) {
			dialog = new ProgressDialog(m);
		}
		//Executed in UI thread
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog.setMessage("Loading Weather Data");
			dialog.show();
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (dialog.isShowing()) {
				dialog.dismiss();
			}
			
			//Toast t = Toast.makeText(getApplicationContext(), 
			//		result, Toast.LENGTH_LONG);
			//t.show();
			
			
			adapter.notifyDataSetChanged();
			lastUpdate = System.currentTimeMillis();
			

			Date d = new Date(lastUpdate);
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy - hh:mm:ss");
	        
			Toast t = Toast.makeText(getApplicationContext(), 
					result + "\nThe information has been refresh since : " + dateFormat.format(d) , 
						Toast.LENGTH_LONG);
			t.show();
			
			
			setTitle(curret_title+" Weather");
		}
		//Executed in BG thread
		@Override
		protected String doInBackground(String... params) {
			BufferedReader in = null;
			StringBuilder buffer = new StringBuilder();
			String line;
			int response;
			try {
				//Get the first parameter, set it as a URL
				URL url = new URL(params[0]);
				//Create a connection to URL
				HttpURLConnection http = (HttpURLConnection)url.openConnection();
				http.setReadTimeout(10000);
				http.setConnectTimeout(15000);
				http.setRequestMethod("GET");
				//We want to download data from URL 
				http.setDoInput(true);
				http.connect();
				
				response = http.getResponseCode();
				if (response == 200) {
					in = new BufferedReader(new InputStreamReader(http.getInputStream()));
					while((line = in.readLine()) != null) {
						buffer.append(line);
					}
					JSONObject json = new JSONObject(buffer.toString());
					JSONObject jmain = json.getJSONObject("main");
					list.clear();
					record = new HashMap<String,String>();
					record.put("name", "Temperature");
					double temp = jmain.getDouble("temp")-273.0;
					record.put("value", String.format(Locale.getDefault(), "%.1f degree celsius", temp));
					list.add(record);
					// add "description"
					//"weather": [{"description":}: "broken...." , ],
					JSONArray jweather = json.getJSONArray("weather");
					JSONObject w0 = jweather.getJSONObject(0);
					String description = w0.getString("description");
					record = new HashMap<String,String>();
					record.put("name", "Description");
					record.put("value", description);
					list.add(record);
					//Pressure
					int pressure = jmain.getInt("pressure");
					record = new HashMap<String,String>();
					record.put("name", "Pressure");
					record.put("value", pressure+" hPa" +
							"");
					list.add(record);
					//humidity
					int humidity = jmain.getInt("humidity");
					record = new HashMap<String,String>();
					record.put("name", "Humidity");
					record.put("value", humidity+" %");
					list.add(record);
					//temp_min
					double tempmin = jmain.getDouble("temp_min")-273.0;
					record = new HashMap<String,String>();
					record.put("name", "Minimum Temperature");
					record.put("value", String.format(Locale.getDefault(), "%.1f degree celsius", tempmin));
					list.add(record);
					//temp_max
					double tempmax = jmain.getDouble("temp_max")-273.0;
					record = new HashMap<String,String>();
					record.put("name", "Maximum Temperature");
					record.put("value", String.format(Locale.getDefault(), "%.1f degree celsius", tempmax));
					list.add(record);
					//wind speed
					JSONObject wind = json.getJSONObject("wind");
					double windsp = wind.getDouble("speed");
					record = new HashMap<String,String>();
					record.put("name", "Wind Speed");
					record.put("value", String.format(Locale.getDefault(), "%.1f mps", windsp));
					list.add(record);
					//wind deg
					double winddeg = wind.getDouble("deg");
					record = new HashMap<String,String>();
					record.put("name", "Wind Deg");
					record.put("value", String.format(Locale.getDefault(), "%.1f Degree", winddeg));
					list.add(record);
					
					return "Finished Loading Weather Data";
				}
				else {
					return "Error "+response;
				}
			} catch (IOException e) {
				return "Error while reading data from the server";
			} catch (JSONException e) {
				return "Error while processing the downloaded data";
			}	
		}
	}

}

